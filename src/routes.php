<?php

Route::get('calculator', function(){
	echo 'Hello from the calculator package!';
});

Route::get('add/{a}/{b}', 'amarsree\Calculator\CalculatorController@add');
Route::get('subtract/{a}/{b}', 'amarsree\Calculator\CalculatorController@subtract');	